
import React, { useState } from "react"
import {useDispatch, useSelector} from "react-redux"

import { selectImage } from "../../store/displayOutput/selectors"

 import { displayOutput } from "../../store/displayOutput/actions"
export default function UploadImage() {
    const dispatch = useDispatch();
    const [image,setImage] = useState()
    const [number,setNumber]= useState()
 

 const imgData = useSelector(selectImage)
    return(
        <div className ="upload">
           
             
               
             
               
               
               {image === undefined ?
                (<div> Upload an image and get the output of the text.
            <div className ="line"></div><h1>Upload an image</h1> <label className="button"><input type="file" name="file" className="inputfile" onClick = {(f)=> {setNumber(f.target.files.item.length)}} onChange={(e)=> { setImage(e.target.files[0])}}  accept="image/*" />Choose File</label></div>)
                :
                (<div><div className ="line"></div>{imgData === null ? 
                    (<div><h1>{number} file added successfully! </h1> <button className="button" onClick={(e)=>{ dispatch(displayOutput(image));}}>Display Output</button></div>)
                    :
                    (<div><h3>Output of {image.name}</h3> <p>  {imgData.response.paragraphs[0].paragraph}</p></div>)}
               
              
               </div>) }
           
         

            <div className ="line"></div>

        </div>
    )
}