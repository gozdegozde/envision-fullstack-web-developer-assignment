import React from 'react';
import './App.css';
import {  Switch, Route } from "react-router-dom"

import UploadImage from "./pages/UploadImage";


function App() {
  return (
    <div className="App">
      <header className="App-header">
         <img src="https://uploads-ssl.webflow.com/5ac150ab7e9bbe927474fbde/5f5a65119f2ea44d8086ce9c_Envision%20Logo%20V2.png" className="App-logo" alt="logo" />
        
      </header>
      
       <Switch>
        
        <Route exact path="/" component={UploadImage} />
       
      
      </Switch>
     
    </div>
  );
}

export default App;

