

const initialState = {
    image: null
}


export default function imageDisplayReducer(state = initialState, action) {
    switch (action.type) {
        case "DISPLAY_IMAGE":
            return {
                image : action.payload
            }

        default:
            return state
    }
}