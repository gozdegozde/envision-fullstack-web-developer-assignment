import axios from 'axios';

export const DISPLAY_IMAGE = "DISPLAY_IMAGE"

const imageUploaded = displayImage => {
  return {
  type : DISPLAY_IMAGE,
  payload : displayImage
  }
}

export const displayOutput = (file) => {



    return async(dispatch) => {

       var formData = new FormData();

  formData.append("photo", file);
  console.log("file",file)
  
  axios.post("https://letsenvision.app/api/test/readDocument", formData, {
      withCredentials: true,
      headers: {'Access-Control-Allow-Origin': '*', 'Content-Type': 'multipart/form-data',
    }
      
    }).then((response) => {
     console.log(response)
     
      fnSuccess(response);
       dispatch(imageUploaded(response.data))
    }).catch((error) => {
      fnFail(error);
    });
   
     

const fnSuccess = (response) => {
  //Add success handling
};

const fnFail = (error) => {
  //Add failed handling
};


    }
   
}