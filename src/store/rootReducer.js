import { combineReducers } from "redux";
 import imageDisplayReducer from "./displayOutput/reducer";

const reducer = combineReducers({
   imageDisplayReducer: imageDisplayReducer
 
});

export default reducer;